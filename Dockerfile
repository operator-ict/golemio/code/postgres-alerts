FROM python:3.11.6-slim

WORKDIR /app

# owner is root
COPY cronpy /etc/cron.d/cronpy

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    cron \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* && \
    groupadd --system -g 1001 nonroot && \
    useradd --system --base-dir /app --uid 1001 --gid nonroot nonroot && \
    chown -R nonroot /app && \
    chmod 644 /etc/cron.d/cronpy && \
    chmod u+s /usr/sbin/cron && \
    chmod 666 /etc/environment


COPY requirements.txt app.py run-as-cron.sh ./
RUN python -m pip install --upgrade pip \
    && pip install -r requirements.txt \
    && touch /var/log/task.log \
    && ln -sf /proc/1/fd/1 /var/log/task.log

COPY --chown=nonroot:nonroot . .
USER nonroot

CMD ["./docker-start.sh"]
