
import unittest

from mock import Mock

from postgres_alerts import utils


class TestUtils(unittest.TestCase):
    def test_check_dependent_migrations(self):
        cur = self.mock_cursor()
        test_migrations_to_run = ['a', 'b', 'c']
        self.assertEqual(utils.check_dependent_migrations(test_migrations_to_run, cur), ['c'])

    def test_check_dependent_migrations_fail(self):
        cur = self.mock_cursor()
        test_migrations_to_run = ['a', 'f']
        self.assertRaises(Exception, utils.check_dependent_migrations, test_migrations_to_run, cur)

    @staticmethod
    def mock_cursor():
        cur = Mock()
        cur.fetchone = Mock(return_value=[None])
        cur.fetchall = Mock(return_value=('a', 'b'))
        return cur


if __name__ == '__main__':
    unittest.main()
