#!/usr/bin/env python3

import datetime
import logging
import os
import sys

from dotenv import load_dotenv

from postgres_alerts.main import process_alerts
from postgres_alerts.migrations import run_db_migrations
from postgres_alerts.utils import load_env

LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO').upper()

logging.basicConfig(
    level=LOG_LEVEL,
    format='%(asctime)s [%(levelname)s] %(message)s',
    handlers=[
        logging.StreamHandler()
    ]
)

if __name__ == '__main__':
    load_dotenv()

    db_credentials = {
        'DB_HOSTNAME': load_env('DB_HOSTNAME'),
        'DB_DATABASE': load_env('DB_DATABASE'),
        'DB_USERNAME': load_env('DB_USERNAME'),
        'DB_PASSWORD': load_env('DB_PASSWORD'),
        'DB_SSLMODE': load_env('DB_SSLMODE', 'prefer'),
        'DB_SSLROOTCERT': load_env('PGSSLROOTCERT', ''),
        'DB_SCHEMA': load_env('DB_SCHEMA', 'alerts')
    }

    if len(sys.argv) > 1 and sys.argv[1] == 'migration':
        run_db_migrations(db_credentials, sys.argv[2])
    else:
        logging.info('Start: {}'.format(datetime.datetime.now()))
        process_alerts(db_credentials)
        logging.info('End: {}'.format(datetime.datetime.now()))
