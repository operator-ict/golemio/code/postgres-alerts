DROP VIEW alerts.v_rules_list;
CREATE VIEW alerts.v_rules_list
AS SELECT r.id,
    last_run.last_run_datetime,
    r.cron,
    r.name,
    r.query_left,
    r.operand,
    r.value_right,
    r.debug
   FROM alerts.rules r
     LEFT JOIN ( SELECT results.rule_id,
            max(results.calculated_at) AS last_run_datetime
           FROM alerts.results
          GROUP BY results.rule_id) last_run ON r.id = last_run.rule_id
  WHERE r.active = true;

ALTER TABLE alerts.rules DROP COLUMN alert_message;
