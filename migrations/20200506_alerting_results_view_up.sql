DROP VIEW IF EXISTS alerts.v_alerting_results;
CREATE VIEW alerts.v_alerting_results
AS SELECT rul.id,
    rul.name,
    res.query,
    res.calculated_at
   FROM alerts.rules rul
     JOIN alerts.results res ON rul.id = res.rule_id
  WHERE res.status::text = 'alerting'::text
  ORDER BY res.calculated_at DESC;