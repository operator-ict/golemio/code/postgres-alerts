CREATE TABLE IF NOT EXISTS alerts.migrations (
	id serial NOT NULL,
	migration_file varchar(500) NOT NULL,
	created_at timestamptz NULL DEFAULT now()
);


CREATE TABLE IF NOT EXISTS alerts.channels (
	id serial NOT NULL,
	"name" varchar(100) NOT NULL,
	description varchar(500) NULL,
	webhook varchar(500) NULL,
	active bool NOT NULL DEFAULT true,
	create_batch_id int8 NULL, -- ID vstupní dávky
	created_at timestamp NULL, -- Čas vložení
	created_by varchar(150) NULL, -- identikace uživatele/procesu, který záznam vložil
	update_batch_id int8 NULL, -- ID poslední dávky, která záznam modifikovala
	updated_at timestamp NULL, -- Čas poslední modifikace
	updated_by varchar(150) NULL, -- Identikace uživatele/procesu, který poslední měnil záznam
	CONSTRAINT channels_pkey PRIMARY KEY (id)
);



CREATE TABLE IF NOT EXISTS alerts.rules (
	id serial NOT NULL,
	"name" varchar(100) NOT NULL,
	id_dataset int4 NOT NULL,
	query_left varchar(5000) NOT NULL,
	operand varchar(5) NULL,
	value_right float8 NULL,
	active bool NOT NULL DEFAULT true,
	debug bool NOT NULL DEFAULT false,
	cron varchar(20) NULL,
	create_batch_id int8 NULL, -- ID vstupní dávky
	created_at timestamp NULL, -- Čas vložení
	created_by varchar(150) NULL, -- identikace uživatele/procesu, který záznam vložil
	update_batch_id int8 NULL, -- ID poslední dávky, která záznam modifikovala
	updated_at timestamp NULL, -- Čas poslední modifikace
	updated_by varchar(150) NULL, -- Identikace uživatele/procesu, který poslední měnil záznam
	CONSTRAINT rules_pkey PRIMARY KEY (id)
);




CREATE TABLE IF NOT EXISTS alerts.results (
	rule_id serial NOT NULL,
	calculated_at timestamp NOT NULL,
	results float8 NOT NULL,
	status varchar(50) NULL,
	duration float8 NULL,
	query varchar(5000) NULL,
	create_batch_id int8 NULL, -- ID vstupní dávky
	created_at timestamp NULL, -- Čas vložení
	created_by varchar(150) NULL, -- identikace uživatele/procesu, který záznam vložil
	update_batch_id int8 NULL, -- ID poslední dávky, která záznam modifikovala
	updated_at timestamp NULL, -- Čas poslední modifikace
	updated_by varchar(150) NULL, -- Identikace uživatele/procesu, který poslední měnil záznam
	CONSTRAINT results_pk PRIMARY KEY (rule_id, calculated_at)
);

ALTER TABLE alerts.results DROP CONSTRAINT IF EXISTS results_fk;
ALTER TABLE alerts.results ADD CONSTRAINT results_fk FOREIGN KEY (rule_id) REFERENCES alerts.rules(id);



CREATE TABLE IF NOT EXISTS alerts.rules_channels (
	rule_id serial NOT NULL,
	channel_id serial NOT NULL,
	create_batch_id int8 NULL, -- ID vstupní dávky
	created_at timestamp NULL, -- Čas vložení
	created_by varchar(150) NULL, -- identikace uživatele/procesu, který záznam vložil
	update_batch_id int8 NULL, -- ID poslední dávky, která záznam modifikovala
	updated_at timestamp NULL, -- Čas poslední modifikace
	updated_by varchar(150) NULL, -- Identikace uživatele/procesu, který poslední měnil záznam
	CONSTRAINT rules_channels_pk PRIMARY KEY (rule_id, channel_id)
);
ALTER TABLE alerts.rules_channels DROP CONSTRAINT IF EXISTS rules_channels_fk;
ALTER TABLE alerts.rules_channels ADD CONSTRAINT rules_channels_fk FOREIGN KEY (channel_id) REFERENCES alerts.channels(id);
ALTER TABLE alerts.rules_channels DROP CONSTRAINT IF EXISTS rules_channels_fk2;
ALTER TABLE alerts.rules_channels ADD CONSTRAINT rules_channels_fk2 FOREIGN KEY (rule_id) REFERENCES alerts.rules(id);



CREATE OR REPLACE VIEW alerts.v_rules_list
AS SELECT r.id,
    last_run.last_run_datetime,
    r.cron,
    r.name,
    r.query_left,
    r.operand,
    r.value_right,
    r.debug
   FROM alerts.rules r
     LEFT JOIN ( SELECT results.rule_id,
            max(results.calculated_at) AS last_run_datetime
           FROM alerts.results
          GROUP BY results.rule_id) last_run ON r.id = last_run.rule_id
  WHERE r.active = true;
