DROP FUNCTION IF EXISTS alerts.check_record_count_yesterday;
CREATE FUNCTION alerts.check_record_count_yesterday(compare_to_number_of_days integer, table_to_check regclass, OUT result numeric)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
BEGIN
   EXECUTE format('with dates as (select
generate_series(date_trunc(''day'', now()) - interval ''30 days''
                     , date_trunc(''day'', now()) - interval ''2 days''
                     , interval  ''1 day'') the_day),
filter_date_workday as (select min(the_day) as filter_day
from (select the_day from dates
where date_part(''dow'', the_day) not in(6, 0) order by 1 desc limit %s) as dates
),
filter_date_weekend as (select min(the_day) as filter_day
from (select the_day from dates
where date_part(''dow'', the_day) in(6, 0) order by 1 desc limit %s) as dates
),
left_part as(
select count(*) as cur_cnt from %s where created_at > now() - interval ''24 hours''
),
right_part_workday as (
select avg(cnt) as cnt_avg from
	(select count(*) as cnt, date_trunc(''day'', created_at) as dt from %s
	where created_at < date_trunc(''day'', now()) - interval ''1 day''
	and created_at >= (select filter_day from filter_date_workday)
	and date_part(''dow'', created_at) not in(6, 0)
	group by date_trunc(''day'', created_at) ) as a
),
right_part_weekend as (
select avg(cnt) as cnt_avg from
	(select count(*) as cnt, date_trunc(''day'', created_at) as dt from %s
	where created_at < date_trunc(''day'', now()) - interval ''1 day''
	and created_at >= (select filter_day from filter_date_weekend)
	and date_part(''dow'', created_at) in(6, 0)
	group by date_trunc(''day'', created_at)) as a
),
right_part as (select case when date_part(''dow'', now() - interval ''24 hours'') < 6 then (select cnt_avg from right_part_workday)
			else (select cnt_avg from right_part_weekend) end as cnt_avg
)
select case when cur_cnt between cnt_avg/2 and cnt_avg*2 then 0 else 1 end from left_part
cross join right_part', compare_to_number_of_days, compare_to_number_of_days, table_to_check, table_to_check, table_to_check)
   INTO result;
END
$function$
;
