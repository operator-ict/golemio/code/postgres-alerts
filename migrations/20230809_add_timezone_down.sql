DROP VIEW IF EXISTS alerts.v_alerting_results;
DROP VIEW IF EXISTS alerts.v_rules_list;

ALTER TABLE "results"
ALTER "calculated_at" TYPE timestamp;

CREATE VIEW alerts.v_alerting_results
AS SELECT rul.id,
          rul.name,
          res.query,
          res.calculated_at
   FROM alerts.rules rul
            JOIN alerts.results res ON rul.id = res.rule_id
   WHERE res.status::text = 'alerting'::text
  ORDER BY res.calculated_at DESC;


CREATE VIEW alerts.v_rules_list
AS SELECT r.id,
          last_run.last_run_datetime,
          r.cron,
          r.name,
          r.query_left,
          r.operand,
          r.value_right,
          r.debug,
          r.alert_message
   FROM alerts.rules r
            LEFT JOIN ( SELECT results.rule_id,
                               max(results.calculated_at) AS last_run_datetime
                        FROM alerts.results
                        GROUP BY results.rule_id) last_run ON r.id = last_run.rule_id
   WHERE r.active = true;