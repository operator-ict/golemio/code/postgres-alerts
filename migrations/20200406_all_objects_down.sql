DROP VIEW IF EXISTS alerts.v_rules_list;

DROP TABLE IF EXISTS alerts.rules_channels;

DROP TABLE IF EXISTS alerts.results;

DROP TABLE IF EXISTS alerts.rules;

DROP TABLE IF EXISTS alerts.channels;

DROP TABLE IF EXISTS alerts.migrations;
