import logging
import time
from datetime import datetime
import pytz

import numexpr
import psycopg2.errors
from croniter import croniter

from . import utils


class Operand:
    def __init__(self, value):
        self.__value = value

    def __str__(self):
        return self.__value

    def to_sql(self):
        return self.__value if self.__value != '=' else '=='


class Status:
    OK = 'ok'
    ALERTING = 'alerting'
    ERROR = 'error'

    def __init__(self, status):
        if status not in [self.OK, self.ALERTING, self.ERROR]:
            raise Exception('Status value "{}" is not valid!'.format(status))
        self.__status = status

    def __str__(self):
        return self.__status

    def is_alerting(self):
        return self.__status == self.ALERTING

    def is_error(self):
        return self.__status == self.ERROR

    @staticmethod
    def ok():
        return Status(Status.OK)

    @staticmethod
    def alerting():
        return Status(Status.ALERTING)

    @staticmethod
    def error():
        return Status(Status.ERROR)


class Result:
    def __init__(self, status: Status, result_left, custom_message, duration):
        self.__status = status
        self.__left = round(result_left, 3) if isinstance(result_left, float) else result_left
        self.__message = custom_message
        self.__duration = round(duration, 3)

    def save(self, rule, cur):
        try:
            cur.execute('insert into alerts.results (rule_id, calculated_at, results, status, duration, query, sql_message) '
                        'VALUES(%s, %s, %s, %s, %s, %s, %s)',
                        (rule.get_id(),
                         datetime.now(pytz.timezone("Europe/Prague")),  # column in pg is timestamp with timezone
                         self.get_left(),
                         self.get_status(),
                         self.get_duration(),
                         rule.log_query(),
                         self.get_message()))
        except psycopg2.Error as ex:
            logging.exception('Failed to insert results for rule ID %s', rule.get_id(), exc_info=ex)
        return 0

    def is_alerting(self):
        return self.__status.is_alerting()

    def is_error(self):
        return self.__status.is_error()

    def get_status(self) -> str:
        return str(self.__status)

    def get_left(self):
        return self.__left

    def get_message(self):
        if self.__message is not None:
            return utils.truncate_message(str(self.__message))
        else:
            return None

    def get_duration(self):
        return self.__duration


class Rule:
    sql_keys = ['id', 'last_run_datetime', 'cron', 'name', 'query_left', 'operand', 'value_right', 'debug',
                'alert_message']

    def __init__(self, id, last_run_datetime, cron, name, query_left, operand, value_right, debug, alert_message):
        self.id_ = id
        self.last_run_datetime = last_run_datetime
        self.cron = cron
        self.name = name
        self.query_left = query_left
        self.operand = Operand(operand)
        self.value_right = value_right
        self.debug = debug
        self.alert_message = alert_message

    def get_id(self) -> str:
        return str(self.id_)

    def process(self, cur):
        if Rule.evaluate(self):
            result = self.calculate_status(self, cur)
            result.save(self, cur)
            if result.is_alerting() or result.is_error():
                utils.send_slack_message(self.id_, self.name, self.alert_message, result.get_message(), cur)

    @staticmethod
    def evaluate(rule) -> bool:
        if rule.last_run_datetime:  # make sure if last run exists, that it is in correct timezone
            cron_base_datetime = rule.last_run_datetime.astimezone(pytz.timezone("Europe/Prague"))
        else:  # make sure if last run is empty that it is initialized with correct timezone
            cron_base_datetime = datetime.now(pytz.timezone("Europe/Prague"))
        cron_base = croniter(rule.cron, cron_base_datetime)
        next_datetime = cron_base.get_next(datetime)
        return (next_datetime < datetime.now(pytz.timezone("Europe/Prague"))) or (rule.last_run_datetime is None)

    @staticmethod
    def calculate_status(rule, cur):
        result_left = 0
        start_time = time.process_time()
        custom_message = None
        try:
            cur.execute(rule.query_left)
            result = cur.fetchone()
            result_left = result[0]
            if len(result) > 1:
                custom_message = result[1]
            expression = str('{!s}{!s}{!s}'.format(result_left, rule.operand.to_sql(), rule.value_right))
            logging.debug("Evaluate expression: {}".format(expression))
            if numexpr.evaluate(
                    expression,
                    local_dict={},
                    global_dict={}
            ).item():
                status = Status.ok()
            else:
                status = Status.alerting()
        except psycopg2.Error as ex:
            logging.exception('Postgres error for rule ID %s', rule.get_id(), exc_info=ex)
            custom_message = f'Postgres error for rule ID {rule.get_id()}, psycopg2.Error: {ex}'
            status = Status.error()
        except KeyError as ex:
            logging.exception('Failed to calculate rule result for rule ID %s', rule.get_id(), exc_info=ex)
            custom_message = f'Failed to calculate rule result for rule ID {rule.get_id()}, KeyError: {ex}'
            status = Status.error()
        except SyntaxError as ex:
            result_left = -1
            logging.exception('Evaluation of rule ID %s failed for expression: %s', rule.get_id(), expression,
                              exc_info=ex)
            custom_message = f'Failed to calculate rule result for rule ID {rule.get_id()}, SyntaxError: {ex}'
            status = Status.error()
        except TypeError as ex:
            result_left = -2
            logging.exception('Evaluation of rule ID %s failed for expression: %s', rule.get_id(), expression,
                              exc_info=ex)
            custom_message = f'Failed to calculate rule result for rule ID {rule.get_id()}, TypeError: {ex}'
            status = Status.error()
        return Result(status, result_left, custom_message, (time.process_time() - start_time))

    def log_query(self):
        return '({}) {} {}'.format(self.query_left, self.operand, str(self.value_right)) if self.debug else None
