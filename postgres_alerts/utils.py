import json
import logging
import os

import psycopg2
import requests


def create_connection(db_credentials):
    """
    Create connection to postgres
    Parameters
    ----------
    db_credentials: array_like

    :raises psycopg2.Error: if failed connection to database
    """

    dsn = (
        "host={} "
        "dbname={} "
        "user={} "
        "password={} "
        "sslmode={} "
        "options='--search_path={}'"
    ).format(
        db_credentials['DB_HOSTNAME'],
        db_credentials['DB_DATABASE'],
        db_credentials['DB_USERNAME'],
        db_credentials['DB_PASSWORD'],
        db_credentials['DB_SSLMODE'],
        db_credentials['DB_SCHEMA']
    )
    if db_credentials['DB_SSLROOTCERT'] != '':
        dsn += " sslrootcert={}".format(db_credentials['DB_SSLROOTCERT'])
    else:
        s = 'PGSSLROOTCERT env variable is not set. Hope it\'s not needed.'
        logging.warning(s)
    conn = psycopg2.connect(dsn)
    conn.autocommit = True
    return conn


def send_slack_message(rule_id: str, rule_name, message, sql_message, cur):
    try:
        cur.execute('select ch.webhook from alerts.rules_channels rch join alerts.channels ch '
                    'on rch.channel_id = ch.id where rch.rule_id = {}'.format(str(rule_id)))
        webhook_urls = cur.fetchall()
    except psycopg2.Error as ex:
        logging.exception('Failed to get list of webhooks for rule ID %s', rule_id, exc_info=ex)
        return
    if webhook_urls:
        try:
            description = message if message is not None else f'Rule {rule_name} failed.'
            sql_message = sql_message if sql_message is not None else ''
            for webhook_url in webhook_urls:
                data = {
                    'text': f'*{description}*. \n\n {sql_message}'
                            f' Query to see details: `select * from alerts.v_alerting_results where id = {rule_id}`'
                }
                requests.post(webhook_url[0], data=json.dumps(
                    data), headers={'Content-Type': 'application/json'})
        except requests.exceptions.RequestException as ex:
            logging.exception('Failed to send a message for alert about rule ID %s, webhook %s', rule_id,
                              webhook_url, exc_info=ex)


def check_dependent_migrations(migrations_list, cur):
    """
    check all items in folder 'migrations' that are placed before the one we want to run and compare with a list of
    executed migrations
    """
    cur.execute('SELECT table_name FROM information_schema.tables WHERE table_schema = \'alerts\' '
                'AND table_name = \'migrations\'')
    migration_table_exists = cur.fetchone()
    if not migration_table_exists:
        return migrations_list
    else:
        cur.execute('SELECT DISTINCT migration_file FROM alerts.migrations WHERE migration_file LIKE \'%up.sql\''
                    'order by migration_file')
        done_migrations = [i[0] for i in cur.fetchall()]
        migrations_to_run = list(set(migrations_list) - set(done_migrations))

    if len(migrations_to_run) == 0:
        return migrations_to_run

    if migrations_list[:len(done_migrations)] != done_migrations:
        raise Exception('You are trying to execute {}, but some of the the depending migrations hasn\'t '
                        'been executed.'.format(str(migrations_to_run)))
    return migrations_to_run


def execute_migrations(migrations_to_run, conn, cur):
    migrations_executed_list = []
    for migration_file in migrations_to_run:
        # run SQL script and insert the migration into migrations table
        with open('migrations/' + migration_file, 'r') as file:
            sql = file.read()
            try:
                cur.execute(sql)
                cur.execute('INSERT INTO alerts.migrations (migration_file) VALUES (\'{}\')'.format(migration_file))
                conn.commit()
                file.close()
                migrations_executed_list.append(migration_file)
            except psycopg2.Error as ex:
                logging.exception('Failed to execute the migration script %s.', migration_file, exc_info=ex)
                raise
    return migrations_executed_list


def load_env(key, default=None):
    if key in os.environ:
        return os.environ.get(key)
    if default is not None:
        return default
    raise ValueError('Key \'{}\' is not set as environment variable!'.format(key))


def truncate_message(string, width=200):
    if len(string) > width:
        string = string[:width-3] + '...'
    return string
