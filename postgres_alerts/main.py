import logging
import sys

import psycopg2

from . import utils
from .rule import Rule


def process_alerts(db_credentials):
    try:
        conn = utils.create_connection(db_credentials)
        cur = conn.cursor()
        cur.execute('select {} from alerts.v_rules_list'.format(', '.join(list(Rule.sql_keys))))
        for rule in cur.fetchall():
            Rule(**dict(zip(Rule.sql_keys, rule))).process(cur)
    except psycopg2.Error as ex:
        logging.exception('Failed to get the list of rules to be run.', exc_info=ex)
        sys.exit(1)
    except RuntimeError as ex:
        logging.exception("Uncaught invalid exception", exc_info=ex)
        sys.exit(1)
    finally:
        cur.close()
        conn.close()


if __name__ == '__main__':
    process_alerts()
