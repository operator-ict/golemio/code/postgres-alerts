import logging
import os
import sys

from postgres_alerts import utils


def run_db_migrations(db_credentials, up_or_down):
    """
    Run database migrations

    Parameters
    ----------
    db_credentials: array_like
    up_or_down: bool
        Direction of migrations.
    """
    conn = utils.create_connection(db_credentials)
    cur = conn.cursor()
    cur.execute('SELECT EXISTS (SELECT FROM information_schema.schemata WHERE schema_name = \'alerts\')')
    schema_exists = cur.fetchone()[0]
    if not schema_exists:
        logging.exception('Schema \'alerts\' must be created first.')
        sys.exit(1)
    migrations_list = sorted([x for x in os.listdir('./migrations') if x.endswith("{}.sql".format(up_or_down))])
    if up_or_down == 'up':
        # check all items in folder 'migrations' that are placed before the one we want to run and compare with a list
        # of executed migrations
        migrations_to_run = utils.check_dependent_migrations(migrations_list, cur)
    migrations_executed_list = utils.execute_migrations(migrations_to_run, conn, cur)
    logging.info('The list of SQL migrations that has just been executed: %s', str(migrations_executed_list))
    cur.close()
    conn.close()
