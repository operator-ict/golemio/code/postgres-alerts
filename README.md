#### Description
This code allows the users to set up monitoring and alerting rules for data in PostreSQL server.

#### How does it work?
The rules are defined in PostrgeSQL table `alerts.rules`. The user is able to configure the rule's logic (SQL script, operand and a value used for the comparison), debug mode (enables the used query to be saved for easier debugging) and the fequency/time for the rule to be validated (cron shedule expression).
Detailed description of this table is below.

All the results are stored in `alerts.results` table. If the validation doesn't fail, the rule gets status "ok". If it does, the status is "alerting" and a Slack message is sent to defined channels. Information about the channels and mapping to rules is in tables alerts.channels and alerts.channels_rules. In case there is an error (psql syntax or code error), there will be an "error" status.

Python application will run on regular basis and check all the cron schedule expression to get a list of rules to be validated at the moment then it runs the whole process for these.


#### PostgreSQL tables in alerts schema

**`alerts.rules`**
- `id` - unique ID, generated automatically
- `name` - name of the rule
- `id_dataset` - ID of related dataset (foreign key to id_dataset in metadata.dataset table)
- `query_left` - SQL query that calculates the result we want to validate
- `operand` - operand used for the validation
- `value_right` - value to compare the result with
- `active` - active flag (boolean, default True)
- `debug` - if True, then SQL query (concatenation of query_left, operand and value_right) is saved in results table (boolean, default False)
- `cron` - cron schedule expression
- `alert_message` - a custom message to be sent in a Slack message in case the rule fails. If it is not defined, the default message will be sent

An example of insert script for the table  `rules`:
```
INSERT INTO alerts.rules ("name", id_dataset, query_left, operand, value_right, active, debug, cron, created_at, alert_message)
VALUES
('python.airbnb_listings -- mising data last month', 3, 'select max(room_lastupdate)::date - current_date from python.airbnb_listings', '<', 30, TRUE, TRUE, '2 7 26 * *', now(), 'Rabín: v tabulce airbnb_listings ještě nejsou data za poslední měsíc.'),
('python.airbnb_occupancy -- mising data last month', 3, 'select max(yearmon)::date - date_trunc(''month'', current_date) from python.airbnb_occupancy', '=', 0, TRUE, TRUE, '2 7 26 * *', now(), 'Rabín: v tabulce airbnb_occupancy ještě nejsou data za poslední měsíc.');
```
If we want an alert that sends an optional message generated based on the sql results (For example to print out all locations that didn't send data in the last 24h). We can pass the message as the second column resulting from the sql statement, the first value will be used to make the comparison based on comparison value and operand, if alert is triggered second value will be send as a message. Minimal example of such a query:

```sql
select 0, 'message generated from sql'
```


**`alerts.results`**
- `rule_id` - ID of the rule
- `calculated_at` - datetime of the validation
- `results` - results of query_left calculation
- `status` - "ok"/"alerting"/"error"
- `duration` - duration of the validation
- `query` - query for debugging in case "debug" field in alerts.rules is set to True
- `sql_message` - optional message passed in the second column of the sql query result

**`alerts.channels`**
- `id` - unique ID of the channel, generated automatically
- `name` - name of the channel
- `description` - description of the channel
- `webhook` - incoming webhook URL
- `active` - boolean

An example of insert script for the table  `channels`:
```
INSERT INTO alerts.channels ("name", description, webhook, active, created_at)
VALUES
('golem_alerts', 'Slack channel for sending alerts about data in Golem', 'https://hooks.slack.com/services/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', true, now())
;
```

**`alerts.channels_rules`**
- `rule_id` - ID of the rule
- `channel_id` - ID of the channel that is mapped to the rule ID

An example of insert script for the table `channels`:
```
INSERT INTO alerts.rules_channels (rule_id, channel_id, created_at)
VALUES
(1, 1, now()),
(2, 1, now())
```

#### Running migrations
At first setup environment variables in your terminal:
```
export DB_HOSTNAME=...
export DB_DATABASE=...
export DB_USERNAME=...
export DB_PASSWORD=...
export DB_SCHEMA=...
```
optionally
```
export DB_SSLMODE=...
export PGSSLROOTCERT=...
```
You have to run database migrations before the first run or after any change in the involved database objects.
The migration scripts are located in `postgres_alerts/migrations` folder. 

You can choose if you want to run 'up' or 'down' migrations. 'Up' migrations mean creating or altering objects, 'down'
migrations mean dropping objects or reverting the changes.

For all the migrations files from the group (up/down) it is checked it the previous (depending) ones were run.

To run them go to the project's folder in terminal and run `python app.py migration up` (or `down`).
To be able to run the `app.py` you need these Python modules installed:
- dotenv
- psycopg2
- numexpr
- croniter

All the migrations you do are stored in `alerts.migration` table.

#### Running tests
Go to the project folder in terminal and run `python -m pytest tests`.

#### Running the process
Go to the project folder in terminal and run `python app.py`.
